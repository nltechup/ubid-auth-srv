﻿using System.ComponentModel.DataAnnotations;

namespace ubid_auth_srv.Models
{
    public class AuthenticateModel
    {
        [Required]
        public string Email { get; set; }

        [Required]
        public string Password { get; set; }
    }
}
