﻿using System.ComponentModel.DataAnnotations;

namespace ubid_auth_srv.Models
{
    public class LoginModel
    {
        [Required]
        public string Email { get; set; }

        [Required]
        public string Password { get; set; }

        public bool RememberMe { get; set; }
    }
}
