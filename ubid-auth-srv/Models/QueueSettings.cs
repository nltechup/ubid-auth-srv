﻿namespace ubid_auth_srv.Models
{
    public class QueueSettings
    {
        public string Rabbit_HostName { get; set; }
        public int Rabbit_Port { get; set; }
        public string Rabbit_Username { get; set; }
        public string Rabbit_Password { get; set; }
    }
}
