﻿using RabbitMQ.Client;
using System;
using System.Threading.Tasks;

namespace ubid_auth_srv.Helpers
{
    public class RabbitMQHelper
    {
        public static void WaitForRabbitMq(ConnectionFactory factory)
        {
            bool started = false;
            while (!started)
            {
                try
                {
                    IConnection connRabbit = factory.CreateConnection();

                    if (connRabbit.IsOpen)
                    {
                        started = true;
                        connRabbit.Close();
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"Error connecting on RabbitMQ: {ex.Message}");
                    Task.Delay(5000).Wait();
                }
            }


        }
    }
}
