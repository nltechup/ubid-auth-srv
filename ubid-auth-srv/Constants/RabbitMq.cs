﻿namespace ubid_auth_srv.Constants
{
    public class RabbitMq
    {
        public static readonly string UBID_Exchange = "uBidExchange";

        public static readonly string User_Create = "user.create";
        public static readonly string User_Update = "user.update";
    }
}
