﻿using System.Collections.Generic;
using ubid_auth_srv.Models;

namespace ubid_auth_srv.Interfaces
{
    public interface IUserService
    {
        User Authenticate(string username, string password);
        IEnumerable<User> GetAll();
    }
}
