﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ubid_auth_srv.Options;

namespace ubid_auth_srv.Services
{
    public static class JwtService
    {
        public static void AddJWTSettings(this IServiceCollection services, IConfiguration configuration)
        {
            var jwtSettings = new JWTSettings();
            configuration.Bind(nameof(jwtSettings), jwtSettings);
            services.AddSingleton(jwtSettings);
        }
    }
}
