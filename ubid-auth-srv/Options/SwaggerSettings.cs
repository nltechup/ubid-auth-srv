﻿namespace ubid_auth_srv.Options
{
    public class SwaggerSettings
    {
        public string JsonRoute { get; set; }
        public string Description { get; set; }
        public string UIEndpoint { get; set; }
    }
}

