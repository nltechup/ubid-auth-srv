﻿namespace ubid_auth_srv.Options
{
    public class JWTSettings
    {
        public string AppKey { get; set; }
        public string ValidIssuer { get; set; }
        public string ValidAudience { get; set; }
        public int ExpireDays { get; set; }
    }
}
