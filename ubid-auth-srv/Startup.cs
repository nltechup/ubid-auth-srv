using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.EntityFrameworkCore;
using ubid_auth_srv.Data;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using ubid_auth_srv.Controllers;
using ubid_auth_srv.Services;
using ubid_auth_srv.Options;
using Microsoft.OpenApi.Models;
using Microsoft.AspNetCore.Http;
using System.Security.Principal;
using ubid_auth_srv.Models;

namespace ubid_auth_srv
{
    public class Startup
    {
        private readonly QueueSettings qSettings = new QueueSettings();
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //services.AddDbContext<ApplicationDbContext>(options =>
            //    options.UseSqlServer(
            //        Configuration.GetConnectionString("DefaultConnection")));

            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlite(Configuration.GetConnectionString("ConnectionName")));

            services.AddDefaultIdentity<IdentityUser>(options => options.SignIn.RequireConfirmedAccount = true)
                .AddEntityFrameworkStores<ApplicationDbContext>();

            //services.AddControllersWithViews();

            //services.AddRazorPages();
            GetQueueSettings();

            var jwtSettings = new JWTSettings();
            Configuration.Bind(nameof(jwtSettings), jwtSettings);


            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(x =>
            {
                x.RequireHttpsMetadata = false;
                x.SaveToken = true;

                x.TokenValidationParameters = new TokenValidationParameters
                {

                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(jwtSettings.AppKey)),
                    ValidateIssuer = true,
                    ValidateAudience = true,
                    ValidateLifetime = true,
                    ValidateIssuerSigningKey = true,
                    ValidIssuer = jwtSettings.ValidIssuer,
                    ValidAudience = jwtSettings.ValidAudience,
                    RequireExpirationTime = false
                };
            });

            services.AddJWTSettings(Configuration);

            services.AddScoped<AuthController>();
            services.AddTransient<IPrincipal>(provider => provider.GetService<IHttpContextAccessor>().HttpContext.User);

            //services.Configure<QueueSettings>(q => Configuration.GetSection("QueueSettings").Bind(q));

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo
                {
                    Version = "v1",
                    Title = "API"
                });

            });
        }

        private void GetQueueSettings()
        {
            QueueSettings q = new QueueSettings();
            Configuration.Bind("QueueSettings", q);

            qSettings.Rabbit_HostName = Environment.GetEnvironmentVariable("RABBIT_HOSTNAME") ?? q.Rabbit_HostName;
            qSettings.Rabbit_Port = Environment.GetEnvironmentVariable("RABBIT_PORT") == null ? q.Rabbit_Port : Convert.ToInt32(Environment.GetEnvironmentVariable("RABBIT_PORT"));
            qSettings.Rabbit_Username = Environment.GetEnvironmentVariable("RABBIT_USERNAME") ?? q.Rabbit_Username;
            qSettings.Rabbit_Password = Environment.GetEnvironmentVariable("RABBIT_PASSWORD") ?? q.Rabbit_Password;
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ApplicationDbContext context, UserManager<IdentityUser> userManager)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            //app.UseHttpsRedirection();

            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            DbSeed.Seed(context, userManager, qSettings);

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
                endpoints.MapRazorPages();
            });

            var swaggerSettings = new SwaggerSettings();
            Configuration.GetSection(nameof(swaggerSettings)).Bind(swaggerSettings);

            app.UseSwagger(o =>
            {
                o.RouteTemplate = swaggerSettings.JsonRoute;
            });
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint(swaggerSettings.UIEndpoint, swaggerSettings.Description);
            });
        }
    }
}
