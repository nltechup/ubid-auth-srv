﻿using Microsoft.AspNetCore.Identity;
using Newtonsoft.Json;
using RabbitMQ.Client;
using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ubid_auth_srv.Helpers;

namespace ubid_auth_srv.Data
{
    public class DbSeed
    {
        internal static void Seed(ApplicationDbContext context, UserManager<IdentityUser> userManager, Models.QueueSettings qSettings)
        {
            context.Database.EnsureCreated();
            SeedUsers(context, userManager, qSettings);
        }

        private static void SeedUsers(ApplicationDbContext context, UserManager<IdentityUser> userManager, Models.QueueSettings _queueSettings)
        {
            if (!context.Users.Any())
            {
                var factory = new ConnectionFactory()
                {
                    HostName = _queueSettings.Rabbit_HostName,
                    Port = _queueSettings.Rabbit_Port,
                    UserName = _queueSettings.Rabbit_Username,
                    Password = _queueSettings.Rabbit_Password,
                    AutomaticRecoveryEnabled = true,
                    RequestedConnectionTimeout = 30000

                };
                RabbitMQHelper.WaitForRabbitMq(factory);

                IConnection _connection = factory.CreateConnection();



                IModel _channel = _connection.CreateModel();

                IdentityUser user1 = new IdentityUser
                {
                    Id = "e384041d-00c3-48c8-92ea-fcdc24d5896e",
                    UserName = "mihai.osvat@nordlogic.com",
                    Email = "mihai.osvat@nordlogic.com",
                    EmailConfirmed = true
                };

                userManager.CreateAsync(user1, "P@rola1!").Wait();
                PublishUser(_channel, user1);

                IdentityUser user2 = new IdentityUser
                {
                    Id = "72e43e4f-df19-4efb-bc56-36fe31ed1a94",
                    UserName = "stefan.bulzan@nordlogic.com",
                    Email = "stefan.bulzan@nordlogic.com",
                    EmailConfirmed = true
                };

                userManager.CreateAsync(user2, "P@rola1!").Wait();
                PublishUser(_channel, user2);

                IdentityUser user3 = new IdentityUser
                {
                    Id = "3e3a27d4-37cd-425e-b936-8266eb532a91",
                    UserName = "paul.mercea@nordlogic.com",
                    Email = "paul.mercea@nordlogic.com",
                    EmailConfirmed = true
                };
                userManager.CreateAsync(user3, "P@rola1!").Wait();
                PublishUser(_channel, user3);

            }
        }

        

        private static void PublishUser(IModel _channel, IdentityUser user)
        {
            var body = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(user));

            _channel.BasicPublish(exchange: Constants.RabbitMq.UBID_Exchange,
                                 routingKey: Constants.RabbitMq.User_Create,
                                 basicProperties: null,
                                 body: body);
        }
    }
}
