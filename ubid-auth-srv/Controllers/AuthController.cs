﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using ubid_auth_srv.Models;
using ubid_auth_srv.Options;

namespace ubid_auth_srv.Controllers
{
    [Route("auth")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly UserManager<IdentityUser> _userManager;
        private readonly IConfiguration _configuration;
        private readonly ILogger<AuthController> _logger;
        private readonly JWTSettings _jwtSettings;
        private readonly SignInManager<IdentityUser> _signinManager;

        public AuthController(UserManager<IdentityUser> userManager, SignInManager<IdentityUser> signinManager, IConfiguration configuration, ILogger<AuthController> logger, JWTSettings jwtSettings)
        {
            this._userManager = userManager;
            this._configuration = configuration;
            this._logger = logger;
            this._jwtSettings = jwtSettings;
            this._signinManager = signinManager;
        }

        [AllowAnonymous]
        [HttpPost("login")]
        //[HttpGet]
        public async Task<ActionResult> Login([FromBody] LoginModel model)
        {
            var user = await this._userManager.FindByNameAsync(model.Email);

            if (user == null)
            {
                return BadRequest(new { message = "Username or password is incorrect!" });
            }

            if (await this._userManager.CheckPasswordAsync(user, model.Password))
            {
                var secretKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_jwtSettings.AppKey));
                var signinCredentials = new SigningCredentials(secretKey, SecurityAlgorithms.HmacSha256);
                IList<Claim> claimsList = await _userManager.GetClaimsAsync(user);

                var claimsAll = new[]
                {
                        new Claim(JwtRegisteredClaimNames.Sub, user.Email),
                        //new Claim("Id", user.Id),
                        //new Claim(JwtRegisteredClaimNames.UniqueName, user.Email),
                        new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                        new Claim(JwtRegisteredClaimNames.Email, user.Email)
                    }.Union(claimsList);



                var token = new JwtSecurityToken(
                    issuer: _jwtSettings.ValidIssuer,
                    audience: _jwtSettings.ValidAudience,
                    claims: claimsAll,
                    expires: DateTime.Now.AddDays(_jwtSettings.ExpireDays),
                    signingCredentials: signinCredentials
                );

                var tokenString = new JwtSecurityTokenHandler().WriteToken(token);

                IdentityResult result = await _userManager.SetAuthenticationTokenAsync(user, "TokenAuth", "access_token", tokenString);

                return Ok(new
                {
                    Data = new
                    {
                        Token = tokenString,
                        Email = user.Email

                    }
                    //Token = tokenString,
                    //ExpiresIn = token.ValidTo,
                    //Username = user.UserName,
                    //Id = user.Id
                });
            }

            return Unauthorized();
        }


        [HttpPost("validatetoken")]
        public async Task<ActionResult> ValidateToken([FromBody] TokenValidationModel token)
        {
            try
            {
                SecurityToken validatedToken;
                JwtSecurityTokenHandler handler = new JwtSecurityTokenHandler();

                TokenValidationParameters validationParameters = new TokenValidationParameters
                {
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(_jwtSettings.AppKey)),
                    ValidateIssuer = true,
                    ValidateAudience = true,
                    ValidateLifetime = true,
                    ValidateIssuerSigningKey = true,
                    ValidIssuer = _jwtSettings.ValidIssuer,
                    ValidAudience = _jwtSettings.ValidAudience,
                    RequireExpirationTime = false
                };


                var userToken = handler.ValidateToken(token.Token, validationParameters, out validatedToken);

                var userId = userToken.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier)?.Value;

                var user = await this._userManager.FindByNameAsync(userId);

                if (user == null)
                {
                    return BadRequest(new { message = "Incorrect token!" });
                }

                return Ok(new { UserId = user.Id });
            }
            catch (SecurityTokenExpiredException stee)
            {
                _logger.LogInformation($"Token expired! - {stee.Message}");
                return BadRequest(new { message = $"Token expired! - {stee.Message}" });
            }
            catch (SecurityTokenException ste)
            {
                _logger.LogInformation($"Token error! - {ste.Message}");
                return BadRequest(new { message = $"Token error! - {ste.Message}" });
            }
            catch (Exception ex)
            {
                _logger.LogInformation($"Validation error: {ex.Message}");
                return BadRequest(new { message = "Incorrect token!" });
            }

        }

        [HttpDelete("logout")]
        public async Task<ActionResult> Logout([FromHeader(Name = "x-user-uuid")] string user_id)
        {
            try
            {
                JwtSecurityTokenHandler handler = new JwtSecurityTokenHandler();

                var user = await this._userManager.FindByIdAsync(user_id);

                if (user == null)
                {
                    //return BadRequest(new { message = "User unknown!" });
                    return BadRequest(true);
                }

                 _userManager.RemoveAuthenticationTokenAsync(user, "TokenAuth", "access_token").Wait();

                return Ok(true);
            }
            catch (Exception ex)
            {
                _logger.LogInformation($"Error deleting token: {ex.Message}");
                return BadRequest(false);
            }

        }

    }
}