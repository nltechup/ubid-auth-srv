﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections;
using System.Collections.Generic;

namespace ubid_auth_srv.Controllers
{
    [Route("env")]
    [ApiController]
    public class EnvController : ControllerBase
    {
        [AllowAnonymous]
        [HttpGet]
        public ActionResult Index()
        {
            Dictionary<string, string> result = new Dictionary<string, string>();
            foreach (DictionaryEntry de in Environment.GetEnvironmentVariables())
            {
                if (de.Key.ToString().StartsWith("RABBIT"))
                {
                    result.Add(de.Key.ToString(), de.Value.ToString());
                }
            }


            return Ok(new
            {
                Data = result
            });
        }
    }
}