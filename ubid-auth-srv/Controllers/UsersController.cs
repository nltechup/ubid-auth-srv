﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using RabbitMQ.Client;
using System.Text;
using ubid_auth_srv.Helpers;
using ubid_auth_srv.Models;
using ubid_auth_srv.Options;

namespace ubid_auth_srv.Controllers
{
    [Route("users")]
    [ApiController]
    [Authorize]
    public class UsersController : ControllerBase
    {
        private readonly UserManager<IdentityUser> _userManager;
        private readonly IConfiguration _configuration;
        private readonly ILogger<AuthController> _logger;
        private readonly JWTSettings _jwtSettings;
        private readonly IModel _channel;
        private readonly QueueSettings _queueSettings = new QueueSettings();

        public UsersController(UserManager<IdentityUser> userManager, IConfiguration configuration, ILogger<AuthController> logger, JWTSettings jwtSettings)
        {
            this._userManager = userManager;
            this._configuration = configuration;
            this._logger = logger;
            this._jwtSettings = jwtSettings;

            configuration.Bind("QueueSettings", _queueSettings);
            var factory = new ConnectionFactory()
            {
                HostName = _queueSettings.Rabbit_HostName,
                Port = _queueSettings.Rabbit_Port,
                UserName = _queueSettings.Rabbit_Username,
                Password = _queueSettings.Rabbit_Password
            };
            RabbitMQHelper.WaitForRabbitMq(factory);

            IConnection _connection = factory.CreateConnection();
            _channel = _connection.CreateModel();
        }

        [AllowAnonymous]
        [HttpPost("register")]
        public IActionResult Register([FromBody]RegistrationModel model)
        {
            if (ModelState.IsValid)
            {
                var user = _userManager.FindByNameAsync(model.Email);

                if (user.Result != null)
                {
                    return BadRequest(new
                    {
                        Data = new
                        {
                            Message = $"User already exists."
                        }
                    });
                }

                IdentityUser newUser = new IdentityUser()
                {
                    UserName = model.Email,
                    Email = model.Email,
                    EmailConfirmed = true
                };

                _userManager.CreateAsync(newUser, model.Password).Wait();
                var body = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(newUser));

                _channel.BasicPublish(exchange: Constants.RabbitMq.UBID_Exchange,
                                     routingKey: Constants.RabbitMq.User_Create,
                                     basicProperties: null,
                                     body: body);
                return Ok(new
                {
                    Data = new
                    {
                        Message = $"{model.Email} was created !"
                    }
                });
            }
            else
            {
                return BadRequest(new
                {
                    Data = new
                    {
                        Message = $"Invalid registration"
                    }
                });
            }
        }
    }
}